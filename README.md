# ci-prototype

A toy project to showcase how CI can be implemented on DZ2 software.
This is not done on the actual DZ2 software but on toy distributed software.

![software overview](softwares.svg "Software overview")
