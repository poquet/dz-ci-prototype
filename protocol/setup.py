#!/usr/bin/env python
from distutils.core import setup
from distutils.spawn import find_executable
from os import makedirs
import subprocess
import sys

protoc = find_executable('protoc')
if protoc is None:
    print('protoc not found', file=sys.stderr)
    sys.exit(1)

makedirs('protocol', exist_ok=True)
protoc_command = [protoc, '--python_out=protocol', 'hello.proto']
if subprocess.call(protoc_command) != 0:
    print('could not generate sources by calling protoc', file=sys.stderr)
    sys.exit(1)

setup(name='protocol',
    version='0.1.0',
    packages=['protocol'],

    python_requires='>=3.6',
    install_requires=[
        'protobuf>=3.19.4,<4',
    ],

    description="Example python protocol library for DZ2 toy CI prototype.",
    author='Millian Poquet',
    author_email='millian.poquet@irit.fr',
    url='https://gitlab.irit.fr/sepia/datazero/ci-prototype',
    license='MIT',
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
    ],
)
