#include <gtest/gtest.h>

#include "compute.hpp"

void test_wrapper_flop_conversion(float desired, float actual)
{
    float got = desired_flops_to_simulated_flops(desired);
    EXPECT_FLOAT_EQ(got, actual);
}

TEST(flop_conversion, small)
{
    test_wrapper_flop_conversion(1.0f, 1.0f);
    test_wrapper_flop_conversion(5.0f, 5.0f);
    test_wrapper_flop_conversion(10.0f, 10.0f);
    test_wrapper_flop_conversion(100.0f, 100.0f);
}

TEST(flop_conversion, big)
{
    test_wrapper_flop_conversion(1e6, 1e6);
    test_wrapper_flop_conversion(1e9, 1e9);
    test_wrapper_flop_conversion(1e18, 1e18);
}

TEST(flop_conversion, wrong)
{
    GTEST_SKIP();
    test_wrapper_flop_conversion(1e26, 1e27);
}
