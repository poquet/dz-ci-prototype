#include <simgrid/s4u.hpp>

float desired_flops_to_simulated_flops(float flops)
{
    return flops * 1.0f;
}
