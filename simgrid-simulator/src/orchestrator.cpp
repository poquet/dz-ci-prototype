#include <simgrid/s4u.hpp>
#include <zmq.h>

#include <protocol/hello.pb.h>

#include "compute.hpp"

XBT_LOG_NEW_DEFAULT_CATEGORY(orchestrator, "The logging channel used by this module");

const char * orchestrator_name(void)
{
    return "orchestrator";
}

void orchestrator(void)
{
    XBT_INFO("started!");

    auto context = zmq_ctx_new();
    xbt_assert(context != nullptr, "cannot create ZMQ context");

    auto socket = zmq_socket(context, ZMQ_REQ);
    xbt_assert(socket != nullptr, "cannot create ZMQ REQ socket (errno=%s)", strerror(errno));

    const char * endpoint = "tcp://localhost:28000";
    XBT_INFO("connecting to endpoint '%s'...", endpoint);
    int err = zmq_connect(socket, endpoint);
    xbt_assert(err == 0, "cannot connect ZMQ socket to '%s' (errno=%s)", endpoint, strerror(errno));
    XBT_INFO("connected!");

    std::string wanna_do_str;
    hello::WhatYouWannaDo wanna_do_buffer;
    wanna_do_buffer.SerializeToString(&wanna_do_str);

    bool bye_received = false;
    while (!bye_received)
    {
        XBT_INFO("sending what you wanna do");
        if (zmq_send(socket, wanna_do_str.data(), wanna_do_str.size(), 0) == -1)
            throw std::runtime_error(std::string("cannot send message on socket (errno=") + strerror(errno) + ")");

        zmq_msg_t msg;
        zmq_msg_init(&msg);
        if (zmq_msg_recv(&msg, socket, 0) == -1)
            throw std::runtime_error(std::string("cannot read message on socket (errno=") + strerror(errno) + ")");

        hello::Order order;
        if (order.ParseFromArray(zmq_msg_data(&msg), zmq_msg_size(&msg)) != true)
            throw std::runtime_error("cannot parse received message as an Order");

        switch (order.order_case())
        {
        case hello::Order::kHello:
            XBT_INFO("received hello from %s", order.hello().name().c_str());
            break;
        case hello::Order::kBye:
            XBT_INFO("received bye");
            bye_received = true;
            break;
        case hello::Order::kComputeFlops: {
            float desired_flops = order.compute_flops().flop_amount();
            float actual_flops = desired_flops_to_simulated_flops(desired_flops);
            XBT_INFO("computing %g flops", actual_flops);
            simgrid::s4u::this_actor::execute(actual_flops);
        } break;
        default:
            throw std::runtime_error("unhandled order case received");
        }
    }

    zmq_close(socket);
    zmq_ctx_destroy(context);
}
