#include <simgrid/s4u.hpp>

#include "orchestrator.hpp"

int main(int argc, char** argv)
{
    if (argc < 2) {
        fprintf(stderr, "usage: %s <PLATFORM>\n", argc > 0 ? argv[0] : "");
        fflush(stderr);
        return 1;
    }

    simgrid::s4u::Engine e(&argc, argv);
    e.load_platform(argv[1]);
    simgrid::s4u::Actor::create(orchestrator_name(), e.host_by_name("Fafard"), &orchestrator);

    e.run();

    return 0;
}
