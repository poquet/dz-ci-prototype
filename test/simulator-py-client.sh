#!/usr/bin/env sh
set -eu

simulator simgrid-simulator/platforms/small_platform.xml 1>simulator.out 2>simulator.err &
simulator_pid=$!

client-py 1>client-py.out 2>client-py.err &
client_pid=$!

wait ${client_pid}
wait ${simulator_pid}

script_dir=$(dirname "$0")
diff simulator.err "${script_dir}/simulator-py-client.expected.err" > simulator.err.diff || true

if [ -s simulator.err.diff ]; then
    # file not empty, there is a diff!
    echo "simulation output is not the expected one, aborting!"
    cat simulator.err.diff
    exit 1
else
    # file is empty, everything is fine
    echo "simulation output is the expected one"
    exit 0
fi
