{ pkgs ? import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/22.05.tar.gz";
    sha256 = "0d643wp3l77hv2pmg2fi7vyxn4rwy0iyr8djcw1h5x72315ck9ik";
  }) {}
}:

let self = rec {
  python3 = pkgs.python3;
  pythonPackages = pkgs.python3Packages;
  simgrid = pkgs.simgrid;
  protocol-cpp = pkgs.stdenv.mkDerivation {
    pname = "protocol-cpp";
    version = "local";
    src = pkgs.lib.sourceByRegex ./protocol [
      "hello\.proto"
      "meson\.build"
    ];
    propagatedBuildInputs = with pkgs; [
      protobuf
    ];
    buildInputs = with pkgs; [
      meson
      pkg-config
      ninja
    ];
  };
  protocol-python = pythonPackages.buildPythonPackage {
    name = "protocol-python-0.1.0";
    src = pkgs.lib.sourceByRegex ./protocol [
      "hello\.proto"
      "setup\.py"
    ];
    propagatedBuildInputs = [
      pkgs.protobuf
      pythonPackages.protobuf
    ];
  };
  simgrid-simulator = pkgs.stdenv.mkDerivation {
    pname = "simgrid-simulator";
    version = "local";
    src = pkgs.lib.sourceByRegex ./simgrid-simulator [
      "meson\.build"
      "meson_options\.txt"
      "src"
      "src/.*\.cpp"
      "src/.*\.hpp"
      "test"
      "test/.*\.cpp"
    ];
    mesonFlags = [ "-Ddo_unit_tests=true" ];
    doCheck = true;
    buildInputs = with pkgs; [
      simgrid
      protocol-cpp
      meson
      pkg-config
      ninja
      zeromq
      gtest.dev
    ];
  };
  client-cpp = pkgs.stdenv.mkDerivation {
    pname = "client-cpp";
    version = "local";
    src = pkgs.lib.sourceByRegex ./client-cpp [
      "meson\.build"
      "src"
      "src/.*\.cpp"
      "src/.*\.hpp"
    ];
    buildInputs = with pkgs; [
      protocol-cpp
      meson
      pkg-config
      ninja
      zeromq
    ];
  };
  client-py = pythonPackages.buildPythonPackage {
    name = "client-py-0.1.0";
    src = pkgs.lib.sourceByRegex ./client-py [
      "setup\.py"
      "client\.py"
    ];
    propagatedBuildInputs = [
      protocol-python
      pythonPackages.protobuf
      pythonPackages.pyzmq
    ];
  };

  client-py_unittest_shell = pkgs.mkShell {
    buildInputs = client-py.propagatedBuildInputs ++ [
      pythonPackages.pytest
    ];
  };
  integration_shell_cpp = pkgs.mkShell {
    buildInputs = [
      simgrid-simulator
      client-cpp
    ];
  };
  integration_shell_py = pkgs.mkShell {
    buildInputs = [
      simgrid-simulator
      client-py
    ];
  };
};
in
  self
