#!/usr/bin/env python
from distutils.core import setup

setup(name='client-py',
    version='0.1.0',
    py_modules=['client'],
    scripts=['client-py'],

    python_requires='>=3.6',
    install_requires=[
        'protocol>=0.1.0',
        'pyzmq>=22.3.0',
        'protobuf>=3.19.4,<4',
    ],

    description="Example python client for DZ2 toy CI prototype.",
    author='Millian Poquet',
    author_email='millian.poquet@irit.fr',
    url='https://gitlab.irit.fr/sepia/datazero/ci-prototype',
    license='MIT',
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
    ],
)
