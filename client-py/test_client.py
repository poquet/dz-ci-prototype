#!/usr/bin/env python3
import client

def test_some_function():
    for i in range(10):
        assert(client.some_function(i) == i*2)
