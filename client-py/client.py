#!/usr/bin/env python3
import zmq
import protocol.hello_pb2 as proto

def some_function(x):
    return x*2

def main():
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind("tcp://*:28000")

    # Round 1: hello
    msg = socket.recv()

    order = proto.Order()
    order.hello.name = 'client-py'
    msg_str = order.SerializeToString()
    socket.send(msg_str)

    # Round2: compute flops
    msg = socket.recv()

    order = proto.Order()
    order.compute_flops.flop_amount = 1e9
    msg_str = order.SerializeToString()
    socket.send(msg_str)

    # Round3: bye
    msg = socket.recv()

    order = proto.Order()
    order.bye.SetInParent()
    msg_str = order.SerializeToString()
    socket.send(msg_str)

if __name__ == '__main__':
    main()
