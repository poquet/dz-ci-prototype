#include "misc.hpp"

#include <zmq.h>

#include <protocol/hello.pb.h>

void client_main_loop(void)
{
    auto context = zmq_ctx_new();
    assert(context != nullptr);

    auto socket = zmq_socket(context, ZMQ_REP);
    assert(socket != nullptr);

    int rc = zmq_bind(socket, "tcp://*:28000");
    assert(rc == 0);

    zmq_msg_t msg;
    hello::Order order;
    std::string msg_buffer_str;

    // Round 1: hello
    zmq_msg_init(&msg);
    if (zmq_msg_recv(&msg, socket, 0) == -1)
        throw std::runtime_error(std::string("cannot read message on socket (errno=") + strerror(errno) + ")");
    zmq_msg_close(&msg);

    order.mutable_hello()->set_name("client-cpp");
    order.SerializeToString(&msg_buffer_str);

    if (zmq_send(socket, msg_buffer_str.data(), msg_buffer_str.size(), 0) == -1)
        throw std::runtime_error(std::string("cannot send message on socket (errno=") + strerror(errno) + ")");

    // Round 2: compute flops
    zmq_msg_init(&msg);
    if (zmq_msg_recv(&msg, socket, 0) == -1)
        throw std::runtime_error(std::string("cannot read message on socket (errno=") + strerror(errno) + ")");
    zmq_msg_close(&msg);

    order.mutable_compute_flops()->set_flop_amount(1e9);
    order.SerializeToString(&msg_buffer_str);

    if (zmq_send(socket, msg_buffer_str.data(), msg_buffer_str.size(), 0) == -1)
        throw std::runtime_error(std::string("cannot send message on socket (errno=") + strerror(errno) + ")");

    // Round 3: bye
    zmq_msg_init(&msg);
    if (zmq_msg_recv(&msg, socket, 0) == -1)
        throw std::runtime_error(std::string("cannot read message on socket (errno=") + strerror(errno) + ")");
    zmq_msg_close(&msg);

    order.mutable_bye();
    order.SerializeToString(&msg_buffer_str);

    if (zmq_send(socket, msg_buffer_str.data(), msg_buffer_str.size(), 0) == -1)
        throw std::runtime_error(std::string("cannot send message on socket (errno=") + strerror(errno) + ")");

    zmq_close(socket);
    zmq_ctx_destroy(context);
}
